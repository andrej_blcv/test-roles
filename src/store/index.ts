import { createStore } from 'vuex';

export default createStore({
  state: {
    role: 'D',
  },
  mutations: {
    setRole(localState, role) {
      // eslint-disable-next-line no-param-reassign
      localState.role = role;
    },
  },
  actions: {
    setRole(context, role) {
      context.commit('setRole', role);
    },
  },
  modules: {
  },
});
