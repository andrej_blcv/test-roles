const roleARoutes = [
  {
    route: '/ra',
    name: 'ra',
  },
  {
    route: '/rb',
    name: 'rb',
  },
  {
    route: '/rc',
    name: 'rc',
  },
  {
    route: '/rd',
    name: 'rd',
  },
  {
    route: '/mr',
    name: 'mr',
  },
  {
    route: '/mg',
    name: 'mg',
  },
];

const roleBRoutes = [
  {
    route: '/rb',
    name: 'rb',
  },
  {
    route: '/mr',
    name: 'mr',
  },
  {
    route: '/mg',
    name: 'mg',
  },
];

const roleCRoutes = [
  {
    route: '/rc',
    name: 'rc',
  },
  {
    route: '/rd',
    name: 'rd',
  },
  {
    route: '/mg',
    name: 'mg',
  },
];

const roleDRoutes = [
  {
    route: '/rd',
    name: 'rd',
  },
  {
    route: '/mr',
    name: 'mr',
  },
  {
    route: '/mg',
    name: 'mg',
  },
  {
    route: '/module',
    name: 'module',
  },
  {
    name: 'aa',
  },
  {
    name: 'bb',
  },
];

const routes: any = {
  A: roleARoutes,
  B: roleBRoutes,
  C: roleCRoutes,
  D: roleDRoutes,
};

function getRoutesForRole(role: string): [] {
  return routes[role];
}

export default getRoutesForRole;
