import { RouteRecordRaw } from 'vue-router';
import router from '../router/index';

import CompA from './component/ModuleComponent.vue';
import CompB from './component/ModuleCompB.vue';

const routes: Array<RouteRecordRaw> = [{
  path: '/module/a',
  name: 'aa',
  component: CompA,
},
{
  path: '/module/b',
  name: 'bb',
  component: CompB,
}];

function addRoutes() {
  router.addRoute('module', routes[0]);
  router.addRoute('module', routes[1]);
}

export default addRoutes;
