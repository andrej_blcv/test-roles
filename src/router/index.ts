import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router';
import Home from '../views/Home.vue';
import store from '../store';
import getRoutesForRole from '../services/configProvider';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/ra',
    name: 'ra',
    component: () => import(/* webpackChunkName: "ra" */ '../views/RoleA.vue'),
  },
  {
    path: '/rb',
    name: 'rb',
    component: () => import(/* webpackChunkName: "rb" */ '../views/RoleB.vue'),
  },
  {
    path: '/rc',
    name: 'rc',
    component: () => import(/* webpackChunkName: "rc" */ '../views/RoleC.vue'),
  },
  {
    path: '/rd',
    name: 'rd',
    component: () => import(/* webpackChunkName: "rd" */ '../views/RoleD.vue'),
  },
  {
    path: '/mr',
    name: 'mr',
    component: () => import(/* webpackChunkName: "mr" */ '../views/MultipleRed.vue'),
  },
  {
    path: '/mg',
    name: 'mg',
    component: () => import(/* webpackChunkName: "mg" */ '../views/MultipleGreen.vue'),
  },
  {
    path: '/not-authorised',
    name: 'not_authorised',
    component: () => import(/* webpackChunkName: "mg" */ '../views/NotPermission.vue'),
  },
  {
    path: '/module',
    name: 'module',
    component: () => import(/* webpackChunkName: "mc" */ '../modules/ModuleContainer.vue'),
    // children: [
    //   {
    //     path: 'a',
    //     name: 'aa',
    //     component: ModuleComA,
    //   },
    //   {
    //     path: 'b',
    //     name: 'bb',
    //     component: ModuleComB,
    //   },
    // ],
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

const openRoutesNames = ['home', 'not_authorised'];

router.beforeEach((to, from, next) => {
  const { role } = store.state;

  if (openRoutesNames.includes(to.name as string)) {
    next();
  } else {
  // needs authorisation
    const supportedRoles = getRoutesForRole(role);
    const isRoolAlowed = supportedRoles.some((r: {name: string}) => to.name === r.name);
    if (!isRoolAlowed) {
      next({ name: 'not_authorised' });
    } else {
      next();
    }
  }
});

export default router;
